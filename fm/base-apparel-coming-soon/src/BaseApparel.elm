module BaseApparel exposing (main, view)

import Browser
import Html exposing (Html, button, div, h1, img, input, p, text)
import Html.Attributes exposing (class, placeholder, value)
import Html.Events exposing (onClick, onInput)
import List
import Validate exposing (Validator, ifBlank, ifInvalidEmail, validate)


type ValidationError
    = EmptyEmail
    | InvalidEmail


type alias Model =
    { email : String
    , validationErrors : List ValidationError
    , invalidEmailSubmitted : Bool
    }


type Msg
    = ChangeEmail String
    | SubmitEmail


modelValidator : Validator ValidationError Model
modelValidator =
    Validate.all
        [ ifBlank .email EmptyEmail
        , ifInvalidEmail .email (always InvalidEmail)
        ]


validationErrors : Model -> List ValidationError
validationErrors model =
    case validate modelValidator model of
        Ok _ ->
            []

        Err errors ->
            errors


main : Program () Model Msg
main =
    let
        initialModel =
            { email = ""
            , validationErrors = []
            , invalidEmailSubmitted = False
            }
    in
    Browser.sandbox
        { init = { initialModel | validationErrors = validationErrors initialModel }
        , update = update
        , view = view
        }


update : Msg -> Model -> Model
update msg model =
    case msg of
        ChangeEmail email ->
            { model
                | email = email
                , validationErrors = validationErrors { model | email = email }
                , invalidEmailSubmitted = False
            }

        SubmitEmail ->
            { model | invalidEmailSubmitted = not <| List.isEmpty model.validationErrors }


view : Model -> Html Msg
view model =
    let
        emailValidationError =
            List.member InvalidEmail model.validationErrors
                && not (List.member EmptyEmail model.validationErrors)
    in
    div [ class """
    flex h-screen text-center sm:text-left font-josefin-sans
    text-base-apparel-coming-soon-desaturated-red
    text-sm sm:text-base
    """ ]
        [ div [ class "w-[375px] sm:w-[1440px] m-auto sm:shadow-2xl" ]
            [ div [ class "flex flex-row" ]
                [ div [ class "relative flex-1" ]
                    [ div [ class "bg-desktop absolute inset-0 -z-10 hidden sm:block" ] []
                    , div [ class "max-w-lg mx-auto" ]
                        [ div [ class "p-8 sm:px-2 sm:py-16" ]
                            [ img [ class "logo-image h-[20px] sm:h-auto object-fit" ] [] ]
                        , img [ class "hero-image-mobile sm:hidden" ] []
                        , div [ class "px-8 sm:px-2 sm:max-w-md" ]
                            [ h1 [ class """
                            mt-16 font-light text-[2.75rem] sm:text-[4rem]
                            leading-none tracking-[0.25em] uppercase
                            """ ]
                                [ text "We're"
                                , div
                                    [ class """
                                    font-semibold
                                    text-base-apparel-coming-soon-dark-grayish-red
                                    """ ]
                                    [ text "coming soon" ]
                                ]
                            , p [ class "mt-4 sm:mt-8" ]
                                [ text
                                    """Hello fellow shoppers! We're currently building our new
                                    fashion store. Add your email below to stay up-to-date with
                                    announcements and our launch deals."""
                                ]
                            , div [ class "relative mt-10 sm:mt-12" ] <|
                                [ input
                                    [ placeholder "Email Address"
                                    , value model.email
                                    , onInput ChangeEmail
                                    , class """
                                    w-full
                                    ring-inset
                                    rounded-full
                                    px-8 py-3 sm:py-4
                                    bg-transparent focus:outline-none
                                    text-base-apparel-coming-soon-dark-grayish-red
                                    font-bold
                                    placeholder:text-base-apparel-coming-soon-desaturated-red
                                    placeholder:opacity-50
                                    """
                                    , if model.invalidEmailSubmitted then
                                        class """
                                        ring-2
                                        ring-base-apparel-coming-soon-soft-red
                                        """

                                      else
                                        class """
                                        ring-1
                                        ring-base-apparel-coming-soon-desaturated-red
                                        ring-opacity-50
                                        """
                                    ]
                                    []
                                , button
                                    [ onClick SubmitEmail
                                    , class """
                                    flex absolute top-0 bottom-0 right-0
                                    px-6 sm:px-9 button-bg rounded-full focus:outline-none
                                    hover:outline hover:outline-2 hover:outline-[hsl(0,_100%,_92%)]
                                    shadow-xl
                                    shadow-base-apparel-coming-soon-desaturated-red/50
                                    hover:shadow-base-apparel-coming-soon-desaturated-red/80
                                    """
                                    ]
                                    (List.append
                                        [ img [ class "arrow-icon object-contain my-auto" ] [] ]
                                     <|
                                        if
                                            model.invalidEmailSubmitted
                                                && (not <| List.isEmpty model.validationErrors)
                                        then
                                            [ div
                                                [ class "flex absolute top-0 bottom-0 -left-10" ]
                                                [ img [ class "error-icon my-auto" ] [] ]
                                            ]

                                        else
                                            []
                                    )
                                ]
                            , div
                                [ class "px-8 mt-2 text-base-apparel-coming-soon-soft-red"
                                , class <|
                                    if emailValidationError then
                                        "visible"

                                    else
                                        "invisible"
                                ]
                                [ text "Please provide a valid email" ]
                            ]
                        ]
                    ]
                , img [ class "hero-image-desktop hidden sm:block object-contain" ] []
                ]
            ]
        ]
