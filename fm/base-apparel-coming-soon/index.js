import { Elm } from "./src/BaseApparel.elm";

const app = Elm.BaseApparel.init({
  node: document.getElementById("root")
});

export default app;
