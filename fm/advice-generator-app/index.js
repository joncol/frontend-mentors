import { Elm } from "./src/AdviceGeneratorApp.elm";

const app = Elm.AdviceGeneratorApp.init({
  node: document.getElementById("root")
});

export default app;
