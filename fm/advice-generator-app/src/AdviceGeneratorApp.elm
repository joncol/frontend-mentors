module AdviceGeneratorApp exposing (main)

import Browser
import Html exposing (Html, button, div, h1, img, text)
import Html.Attributes exposing (class)
import Html.Events exposing (onClick)
import Http
import Json.Decode as D exposing (Decoder, field, int, string)
import PingComingSoonPage exposing (update)
import Task
import Time



-- MAIN


main : Program () Model Msg
main =
    Browser.element
        { init = init
        , update = update
        , subscriptions = subscriptions
        , view = view
        }



-- MODEL


type Model
    = Failure
    | Loading (Maybe Advice)
    | Success Advice


init : () -> ( Model, Cmd Msg )
init _ =
    ( Loading Nothing
    , getAdvice
    )



-- UPDATE


type alias Advice =
    { id : Int
    , advice : String
    }


type Msg
    = GetAdvice
    | GotAdvice (Result Http.Error Advice)


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        GetAdvice ->
            let
                prevAdvice =
                    case model of
                        Success advice ->
                            Just advice

                        _ ->
                            Nothing
            in
            ( Loading prevAdvice, getAdvice )

        GotAdvice result ->
            case result of
                Ok advice ->
                    ( Success advice, Cmd.none )

                Err _ ->
                    ( Failure, Cmd.none )



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions _ =
    Sub.none



-- VIEW


view : Model -> Html Msg
view model =
    case model of
        Success advice ->
            viewAdvice advice

        Loading prevAdvice ->
            case prevAdvice of
                Just advice ->
                    viewAdvice advice

                Nothing ->
                    div [] []

        Failure ->
            div [] [ text "Some error happened" ]


viewAdvice : Advice -> Html Msg
viewAdvice advice =
    div
        [ class "flex h-screen font-manrope font-extrabold"
        , class "text-[rgb(207,227,234)] text-[28px] text-center"
        ]
        [ div [ class "w-[375px] sm:w-[540px] m-auto" ]
            [ div
                [ class "relative flex flex-col justify-center items-center"
                , class "mx-4 p-8 pb-6"
                , class "bg-advice-generator-app-dark-grayish-blue rounded-xl"
                ]
                [ div []
                    [ h1
                        [ class "uppercase text-advice-generator-app-neon-green text-xs"
                        , class "tracking-[0.35em] mb-5"
                        ]
                        [ text <| "Advice #" ++ String.fromInt advice.id ]
                    , text <| "“" ++ advice.advice ++ "”"
                    ]
                , div []
                    [ img [ class "mt-6 pattern-divider" ] [] ]
                , div []
                    [ button [ onClick GetAdvice ]
                        [ div
                            [ class "absolute bottom-0"
                            , class "-translate-x-1/2 translate-y-1/2 w-[64px] h-[64px]"
                            , class "bg-advice-generator-app-neon-green rounded-full"
                            , class "hover:shadow-[0_0_20px_5px_rgba(83,255,171,0.5)]"
                            ]
                            [ div []
                                [ img [ class "absolute inset-0 m-auto dice" ] [] ]
                            ]
                        ]
                    ]
                ]
            ]
        ]



-- HTTP


getAdvice : Cmd Msg
getAdvice =
    let
        resolver =
            Http.stringResolver <| handleJsonResponse adviceDecode

        getNewAdvice currentTime =
            Http.task
                { method = "GET"
                , headers = []
                , url = "https://api.adviceslip.com/advice?t=" ++ String.fromInt (Time.posixToMillis currentTime)
                , body = Http.emptyBody
                , resolver = resolver
                , timeout = Nothing
                }

        makeRequest =
            Time.now |> Task.andThen getNewAdvice
    in
    Task.attempt GotAdvice makeRequest


handleJsonResponse : Decoder a -> Http.Response String -> Result Http.Error a
handleJsonResponse decoder response =
    case response of
        Http.BadUrl_ url ->
            Err (Http.BadUrl url)

        Http.Timeout_ ->
            Err Http.Timeout

        Http.NetworkError_ ->
            Err Http.NetworkError

        Http.BadStatus_ metadata _ ->
            Err (Http.BadStatus metadata.statusCode)

        Http.GoodStatus_ _ body ->
            case D.decodeString decoder body of
                Ok value ->
                    Ok value

                Err err ->
                    Err (Http.BadBody <| D.errorToString err)


adviceDecode : Decoder Advice
adviceDecode =
    D.map2 Advice
        (field "slip" <|
            field "id" int
        )
        (field "slip" <|
            field "advice" string
        )
