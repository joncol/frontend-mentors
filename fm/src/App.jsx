import React from "react";

import solutions from "./solutions.json";

class App extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <>
        <ul>
          {solutions.map((s) => (
            <li key={s.name}>
              <a href={"/" + s.dir + "/index.html"}>{s.name}</a>: {s.description}
            </li>
          ))}
        </ul>
      </>
    );
  }
}

export default App;
