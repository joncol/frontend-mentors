port module TimeTrackingDashboard exposing (main)

import Array exposing (Array)
import Browser
import Html exposing (Attribute, Html, article, button, div, h1, h3, img, section, text)
import Html.Attributes exposing (class)
import Html.Events exposing (onClick)



-- MAIN


main : Program () Model Msg
main =
    Browser.element
        { init = init
        , update = update
        , subscriptions = subscriptions
        , view = view
        }



-- PORTS


port receiveData : (Array ActivityRecord -> msg) -> Sub msg



-- MODEL


type alias Model =
    { timeFrame : Timeframe
    , activityData : Array ActivityRecord
    }


init : () -> ( Model, Cmd Msg )
init _ =
    ( { timeFrame = Daily
      , activityData = Array.empty
      }
    , Cmd.none
    )


type Timeframe
    = Daily
    | Weekly
    | Monthly


type TimeType
    = CurrentTime
    | PreviousTime


type alias ActivityRecord =
    { title : String
    , timeframes : TimeframesRecord
    }


type alias TimeframesRecord =
    { daily : TimeframeRecord
    , weekly : TimeframeRecord
    , monthly : TimeframeRecord
    }


getTimeframe : TimeframesRecord -> Timeframe -> TimeframeRecord
getTimeframe timeframes timeframe =
    case timeframe of
        Daily ->
            timeframes.daily

        Weekly ->
            timeframes.weekly

        Monthly ->
            timeframes.monthly


type alias TimeframeRecord =
    { current : Int
    , previous : Int
    }


type ActivityCategory
    = Work
    | Play
    | Study
    | Exercise
    | Social
    | SelfCare


activityCategoryIndex : ActivityCategory -> Int
activityCategoryIndex category =
    case category of
        Work ->
            0

        Play ->
            1

        Study ->
            2

        Exercise ->
            3

        Social ->
            4

        SelfCare ->
            5


activityCategoryName : ActivityCategory -> String
activityCategoryName category =
    case category of
        Work ->
            "Work"

        Play ->
            "Play"

        Study ->
            "Study"

        Exercise ->
            "Exercise"

        Social ->
            "Social"

        SelfCare ->
            "Self Care"


allActivityCategories : List ActivityCategory
allActivityCategories =
    [ Work
    , Play
    , Study
    , Exercise
    , Social
    , SelfCare
    ]


previousTimeframeString : Timeframe -> String
previousTimeframeString timeFrame =
    case timeFrame of
        Daily ->
            "Yesterday"

        Weekly ->
            "Last Week"

        Monthly ->
            "Last Month"



-- UPDATE


type Msg
    = ChangeTimeframe Timeframe
    | DataChanged (Array ActivityRecord)


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        ChangeTimeframe timeFrame ->
            ( { model | timeFrame = timeFrame }, Cmd.none )

        DataChanged data ->
            ( { model | activityData = data }, Cmd.none )



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions _ =
    receiveData DataChanged



-- VIEW


view : Model -> Html Msg
view model =
    div
        [ class "flex sm:h-screen font-rubik text-white" ]
        [ div [ class "w-[375px] sm:min-w-max m-auto" ]
            [ article
                [ class "grid gap-6 sm:h-[517px] sm:max-w-max m-4"
                , class "sm:grid-cols-4 sm:grid-rows-2"
                ]
              <|
                viewProfileCard model
                    :: List.map (viewActivityCard model) allActivityCategories
            ]
        ]


viewProfileCard : Model -> Html Msg
viewProfileCard model =
    section
        [ class "flex flex-col justify-between sm:w-[255px] sm:row-span-2"
        , class "bg-time-tracking-dashboard-dark-blue"
        , class "rounded-2xl"
        ]
        [ section
            [ class "grow p-6 sm:p-8"
            , class "bg-time-tracking-dashboard-blue"
            , class "rounded-2xl"
            ]
            [ div [ class "flex flex-row sm:flex-col gap-4 sm:gap-12" ]
                [ div [ class "w-14 sm:w-24" ]
                    [ img
                        [ class "profile-picture"
                        , class "ring-2 sm:ring-[3px] ring-white rounded-full"
                        ]
                        []
                    ]
                , div
                    []
                    [ h3 [ class "sm:text-lg text-time-tracking-dashboard-pale-blue" ]
                        [ text "Report for" ]
                    , h1 [ class "text-xl sm:text-[2.5rem] sm:leading-[2.75rem] font-light" ]
                        [ text "Jeremy Robson" ]
                    ]
                ]
            ]
        , section
            [ class "flex flex-row justify-between sm:flex-col sm:items-start sm:gap-4 p-6 sm:px-8"
            , class "text-time-tracking-dashboard-desaturated-blue"
            , class "text-[18px]"
            ]
            [ button
                [ onClick <| ChangeTimeframe Daily
                , class "hover:text-white transition"
                , timeFrameHeaderClass model Daily
                ]
                [ text "Daily" ]
            , button
                [ onClick <| ChangeTimeframe Weekly
                , class "hover:text-white transition"
                , timeFrameHeaderClass model Weekly
                ]
                [ text "Weekly" ]
            , button
                [ onClick <| ChangeTimeframe Monthly
                , class "hover:text-white transition"
                , timeFrameHeaderClass model Monthly
                ]
                [ text "Monthly" ]
            ]
        ]


viewActivityCard : Model -> ActivityCategory -> Html Msg
viewActivityCard model category =
    section
        [ class "sm:w-[255px]"
        , class "flex flex-col"
        ]
        [ section
            [ class "px-4 rounded-t-2xl"
            , activityCategoryBackgroundClass category
            ]
            [ div
                [ class "w-full h-[64px] overflow-hidden" ]
                [ img
                    [ class "float-right -translate-y-[8%]"
                    , activityCategoryIconClass category
                    ]
                    []
                ]
            ]
        , button
            [ class "grow grid grid-cols-2 grid-rows-2 sm:grid-rows-3 grid-flow-row"
            , class "text-left items-center"
            , class "p-6 sm:py-2 -mt-[24px] relative z-10"
            , class "hover:bg-[rgb(52,57,123)]"
            , class "bg-time-tracking-dashboard-dark-blue"
            , class "rounded-2xl"
            , class "text-time-tracking-dashboard-pale-blue"
            ]
            [ div
                [ class "text-[18px] font-medium text-white"
                ]
                [ text <| activityCategoryName category ]
            , div [ class "flex justify-self-end h-4" ]
                [ img [ class "icon-ellipsis m-auto" ] [] ]
            , div
                [ class "text-[2rem] sm:text-6xl font-light text-white"
                , class "sm:col-span-2"
                ]
                [ text <| currentActivityTimeString model category CurrentTime ++ "hrs" ]
            , div
                [ class "min-w-max justify-self-end sm:justify-self-start text-base"
                , class "sm:col-span-2"
                ]
                [ text <|
                    previousTimeframeString model.timeFrame
                        ++ " - "
                        ++ currentActivityTimeString model category PreviousTime
                        ++ "hrs"
                ]
            ]
        ]


timeFrameHeaderClass : Model -> Timeframe -> Attribute msg
timeFrameHeaderClass model timeFrame =
    class <|
        if model.timeFrame == timeFrame then
            "text-white"

        else
            ""


activityCategoryBackgroundClass : ActivityCategory -> Attribute msg
activityCategoryBackgroundClass category =
    case category of
        Work ->
            class "bg-time-tracking-dashboard-work-light-red"

        Play ->
            class "bg-time-tracking-dashboard-play-soft-blue"

        Study ->
            class "bg-time-tracking-dashboard-study-light-red"

        Exercise ->
            class "bg-time-tracking-dashboard-exercise-lime-green"

        Social ->
            class "bg-time-tracking-dashboard-social-violet"

        SelfCare ->
            class "bg-time-tracking-dashboard-self-care-soft-orange"


activityCategoryIconClass : ActivityCategory -> Attribute msg
activityCategoryIconClass category =
    case category of
        Work ->
            class "icon-work"

        Play ->
            class "icon-play"

        Study ->
            class "icon-study"

        Exercise ->
            class "icon-exercise"

        Social ->
            class "icon-social"

        SelfCare ->
            class "icon-self-care"


currentActivityTimeString : Model -> ActivityCategory -> TimeType -> String
currentActivityTimeString model category timeType =
    let
        index =
            activityCategoryIndex category

        timeTypeAccessor =
            case timeType of
                CurrentTime ->
                    .current

                PreviousTime ->
                    .previous
    in
    Array.get index model.activityData
        |> Maybe.andThen (\record -> Just (getTimeframe record.timeframes model.timeFrame |> timeTypeAccessor))
        |> Maybe.withDefault 0
        |> String.fromInt
