import { Elm } from "./src/TimeTrackingDashboard.elm";
import data from "./data.json";

const app = Elm.TimeTrackingDashboard.init({
  node: document.getElementById("root")
});

app.ports.receiveData.send(data);

export default app;
