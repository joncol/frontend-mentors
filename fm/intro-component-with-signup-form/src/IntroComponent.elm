module IntroComponent exposing (main)

import Browser
import Dict exposing (Dict)
import Html exposing (Html, div, fieldset, form, h1, img, input, p, span, text)
import Html.Attributes exposing (class, disabled, placeholder, type_, value)
import Html.Events exposing (onInput, onSubmit)
import Set exposing (Set)
import Validate exposing (Validator, ifBlank, ifInvalidEmail, validate)


type InputField
    = FirstName
    | LastName
    | Email
    | Password


allFields : List InputField
allFields =
    [ FirstName
    , LastName
    , Email
    , Password
    ]


fieldName : InputField -> String
fieldName field =
    case field of
        FirstName ->
            "First Name"

        LastName ->
            "Last Name"

        Email ->
            "Email Address"

        Password ->
            "Password"


type ValidationError
    = EmptyValue InputField
    | InvalidEmail


main : Program () Model Msg
main =
    Browser.sandbox
        { init = init
        , view = view
        , update = update
        }


modelValidator : Validator ValidationError Model
modelValidator =
    Validate.all
        [ ifBlank (getField FirstName) <| EmptyValue FirstName
        , ifBlank (getField LastName) <| EmptyValue LastName
        , Validate.firstError
            [ ifBlank (getField Email) <| EmptyValue Email
            , ifInvalidEmail (getField Email) (always InvalidEmail)
            ]
        , ifBlank (getField Password) <| EmptyValue Password
        ]


validationErrors : Model -> List ValidationError
validationErrors model =
    case validate modelValidator model of
        Ok _ ->
            []

        Err errors ->
            errors


type alias Model =
    { fields : Dict String String
    , userRegistered : Bool
    , validationErrors : List ValidationError
    , justSubmitted : Set String
    }


getField : InputField -> Model -> String
getField field model =
    Maybe.withDefault "" <| Dict.get (fieldName field) model.fields


init : Model
init =
    let
        model =
            { fields = Dict.empty
            , userRegistered = False
            , validationErrors = []
            , justSubmitted = Set.empty
            }
    in
    { model | validationErrors = validationErrors model }


type Msg
    = ChangeField InputField String
    | SubmitForm


update : Msg -> Model -> Model
update msg model =
    let
        updatedModel =
            case msg of
                ChangeField field v ->
                    let
                        name =
                            fieldName field
                    in
                    { model
                        | fields = Dict.insert name v model.fields
                        , justSubmitted =
                            Set.remove name
                                model.justSubmitted
                    }

                SubmitForm ->
                    { model
                        | userRegistered = List.isEmpty model.validationErrors
                        , justSubmitted =
                            Set.fromList <|
                                List.map fieldName allFields
                    }
    in
    { updatedModel | validationErrors = validationErrors updatedModel }


view : Model -> Html Msg
view model =
    div
        [ class "flex h-screen items-center"
        , class "font-poppins text-center sm:text-left text-white"
        ]
        [ div
            [ class "fixed top-1/2 left-1/2 -mr-1/2 -translate-x-1/2 -translate-y-1/2 w-min p-6"
            , class "z-10 bg-intro-component-with-signup-form-blue text-2xl"
            , class "text-white rounded-xl uppercase tracking-widest"
            , class "font-medium shadow-[0_8px_0] shadow-red-900/30"
            , class <|
                if model.userRegistered then
                    ""

                else
                    "hidden"
            ]
            [ text "Welcome" ]
        , div
            [ class "flex flex-col sm:flex-row sm:justify-center sm:items-center"
            , class "w-[375px] sm:w-[1440px] sm:h-[800px]"
            , class "m-auto pt-24 pb-16 sm:py-4 px-6"
            , class "bg"
            , class <|
                if model.userRegistered then
                    "opacity-20"

                else
                    ""
            ]
            [ div [ class "sm:max-w-[540px] sm:mr-6" ]
                [ h1 [ class "text-3xl sm:text-5xl mb-8 font-bold" ]
                    [ text "Learn to code by watching others" ]
                , p [ class "font-medium" ]
                    [ text
                        """
                        See how experienced developers solve problems in real-time.
                        Watching scripted tutorials is great, but understanding how
                        developers think is invaluable.
                        """
                    ]
                ]
            , div
                [ class "sm:min-w-[327px] lg:min-w-[540px]" ]
                [ div
                    [ class "mt-14 sm:mt-0 py-6 px-16 sm:px-0"
                    , class "bg-intro-component-with-signup-form-blue rounded-lg"
                    , class "shadow-[0_8px_0] shadow-red-900/30"
                    ]
                    [ p [ class "sm:text-center" ]
                        [ span [ class "font-bold" ]
                            [ text "Try it free 7 days" ]
                        , text " then $20/mo. thereafter"
                        ]
                    ]
                , div
                    [ class "mt-6 p-6 sm:p-10 pb-8 w-full bg-white rounded-lg"
                    , class "shadow-[0_8px_0] shadow-red-900/30"
                    ]
                    [ form
                        [ class "rounded-lg"
                        , class "text-intro-component-with-signup-form-dark-blue"
                        , onSubmit SubmitForm
                        ]
                        [ fieldset [ class "space-y-5", disabled model.userRegistered ] <|
                            List.append
                                (List.map (fieldAndValidation model) allFields)
                                [ input
                                    [ type_ "submit"
                                    , value "Claim your free trial"
                                    , disabled model.userRegistered
                                    , class "w-full py-4"
                                    , class "bg-intro-component-with-signup-form-green"
                                    , class <|
                                        if model.userRegistered then
                                            ""

                                        else
                                            "hover:bg-emerald-300 focus:outline-0 focus:bg-emerald-300"
                                    , class "text-white rounded-lg uppercase"
                                    , class "font-semibold tracking-wide"
                                    , class "shadow-[0_4px_0] shadow-green-700/80"
                                    ]
                                    []
                                , p
                                    [ class "px-2 text-intro-component-with-signup-form-grayish-blue"
                                    , class "text-[0.65rem] font-medium text-center"
                                    ]
                                    [ text "By clicking the button, you are agreeing to our "
                                    , span [ class "font-semibold text-intro-component-with-signup-form-red" ]
                                        [ text "Terms and Services" ]
                                    ]
                                ]
                        ]
                    ]
                ]
            ]
        ]


fieldAndValidation : Model -> InputField -> Html Msg
fieldAndValidation model field =
    let
        isValid =
            isFieldValid model field

        classes =
            if isValid then
                class ""

            else
                class "border-[3px] border-intro-component-with-signup-form-red"
    in
    div []
        [ div [ class "relative" ]
            [ input
                [ type_ <|
                    case field of
                        Password ->
                            "password"

                        _ ->
                            "text"
                , placeholder <| fieldName field
                , value <| getField field model
                , onInput <| ChangeField field
                , classes
                ]
                []
            , div
                [ class "flex absolute top-0 bottom-0 right-6"
                , if isValid then
                    class "hidden"

                  else
                    class ""
                ]
                [ div [ class "my-auto" ] [ img [ class "error-icon" ] [] ] ]
            ]
        , fieldValidationError model field
        ]


isFieldValid : Model -> InputField -> Bool
isFieldValid model field =
    (not <| Set.member (fieldName field) model.justSubmitted)
        || ((not <| List.member (EmptyValue field) model.validationErrors)
                && (not <|
                        (field
                            == Email
                            && List.member InvalidEmail model.validationErrors
                        )
                   )
           )


fieldValidationError : Model -> InputField -> Html msg
fieldValidationError model field =
    let
        classes =
            [ class "mt-1 text-[0.7rem] text-right"
            , class "font-semibold text-intro-component-with-signup-form-red italic"
            ]
    in
    if not <| Set.member (fieldName field) model.justSubmitted then
        div [] []

    else if List.member (EmptyValue field) model.validationErrors then
        div
            classes
            [ text <| fieldName field ++ " cannot be empty" ]

    else if field == Email && List.member InvalidEmail model.validationErrors then
        div
            classes
            [ text "Looks like this is not an email" ]

    else
        div [] []
